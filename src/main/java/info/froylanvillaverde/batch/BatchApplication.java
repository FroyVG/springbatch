package info.froylanvillaverde.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchApplication {

    public static void main(String[] args) {
        System.exit(SpringApplication.exit(//Ensure that the JVM exits upon job completion
                SpringApplication.run(BatchApplication.class, args)
        ));
    }

}
