package info.froylanvillaverde.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component("DefaultStepExecutionListener")
public class StepCompletionNotificacionListener extends StepExecutionListenerSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(StepCompletionNotificacionListener.class);

    @Override
    public void beforeStep(StepExecution stepExecution) {
        LOGGER.info("STARTING STEP");
        LOGGER.info("StepName: " + stepExecution.getStepName());
        LOGGER.info("Status: " + stepExecution.getStatus());
        LOGGER.info("ID: " + stepExecution.getId());
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        LOGGER.info("STEP COMPLETED!!!");
        LOGGER.info("Start time: " + stepExecution.getStartTime());
        LOGGER.info("End Time: " + stepExecution.getEndTime());
        LOGGER.info("ID: " + stepExecution.getId());
        return super.afterStep(stepExecution);
    }
}
